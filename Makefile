#
# NetSurf test site build makefile

DESTDIR?=/tmp

.PHONY:install all

all:
	@echo "install target is probably what you want"

install:
	mkdir -p "${DESTDIR}/html" "${DESTDIR}/cgi-bin"
	cp -a html monkey-test "${DESTDIR}"/html/
	cp -a cgi-bin/* "${DESTDIR}"/cgi-bin/
