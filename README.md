NetSurf test suite
==================

This is a collection of tests for the NetSurf web browser.

| Directory     | Description                                    |
| ------------- | ---------------------------------------------- |
| `legacy`      | Old tests that pre-date automated testing      |
| `monkey-test` | Automated tests for NetSurf's monkey front end |