#!/usr/bin/python3

'''
NetSurf test ordered list generator

The liststyle form parameter may be given to select different list style types.
'''

import os
import re
import cgi
import cgitb

cgitb.enable()

def main():
    '''
    The test plan generator
    '''
    docroot = os.environ["DOCUMENT_ROOT"]

    testroot = os.path.join(docroot, "monkey-test")

    params = cgi.FieldStorage()

    liststyle = 'decimal'
    listcount = 1000
    liststart = 1
    listreverse = 0

    # get cgi parameters
    if 'liststyle' in params and re.match('^[A-Za-z0-9-]+$', params['liststyle'].value):
        liststyle = params['liststyle'].value

    if 'listcount' in params and re.match('^[0-9]+$', params['listcount'].value):
        listcount = int(params['listcount'].value)

    if 'liststart' in params and re.match('^[0-9-]+$', params['liststart'].value):
        liststart = int(params['liststart'].value)

    if 'listreverse' in params and re.match('^[0-1]+$', params['listreverse'].value):
        listreverse = int(params['listreverse'].value)

    # ensure count is reasonable
    if listcount > 100000:
        listcount = 100000
        
    print('Content-Type: text/html')
    print('')

    print('<!DOCTYPE html>')
    print('<html>')
    print('<head>')
    print('<style>')
    print('ol.a {list-style-type:',liststyle,';}')
    print('</style>')
    print('</head>')
    print('<body>')
    print('<h1>ordered list marker test with',liststyle,'style</h1>')
    print('<ol class="a"', end='')
    if liststart != 1:
        print(' start="{}"'.format(liststart), end='')
    if listreverse != 0:
        print(' reversed', end='')
    print('>')
    for num in range(liststart, (liststart + listcount)):
        print('<li>',num,'</li>', sep="")
    
    print('</ol>')
    print('</body>')
    print('</html>')

if __name__ == "__main__":
    main()
