#!/usr/bin/python3

from urllib.parse import parse_qs
import cgi
import cgitb
import os
import time

cgitb.enable()

qs = parse_qs(os.getenv("QUERY_STRING", "t=1&val=async-cb-adds-timeout"))

content = qs.get("val", ["async-cb-adds-timeout"])[0]

print("Content-Type: text/javascript")
print("")

sleep_time = int(qs.get("t", [1])[0])

time.sleep(sleep_time)

with open("and-then-js-{}.js".format(content)) as fh:
    print(fh.read())
