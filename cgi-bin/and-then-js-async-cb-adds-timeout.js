/* JS returned by and-then-js.cgi
 * this is to be loaded as an async script
 */

/* After one second */
setTimeout(function() {
    /* Add a <script> tag */
    var script = document.createElement("SCRIPT");
    script.src = "https://test.netsurf-browser.org/html/hello-world.js";
    document.body.appendChild(script);
}, 1000);
