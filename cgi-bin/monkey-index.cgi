#!/usr/bin/python3

'''
NetSurf test plan generator

The division form parameter may be given to select different groups of tests
  to include.
'''

import os
import re
import cgi
import cgitb
import yaml

cgitb.enable()

def main():
    '''
    The test plan generator
    '''
    docroot = os.environ["DOCUMENT_ROOT"]

    files = {}

    testroot = os.path.join(docroot, "monkey-test")

    params = cgi.FieldStorage()

    division = 'default'
    group_filter = None

    if 'division' in params and re.match('^[A-Za-z0-9-]+$', params['division'].value):
        division = params['division'].value

    if 'group' in params and re.match('^[A-Za-z0-9-]+$', params['group'].value):
        group_filter = params['group'].value

    print('Content-Type: text/plain')
    print('')

    if not os.path.isdir(testroot + '/' + division):
        print('# Division ' + division + ' not found')
        return

    flist = [f for f in os.listdir(testroot) if f.endswith('.yaml')]
    flist.extend(os.path.join(division, f) for f in os.listdir(testroot + '/' + division) if f.endswith('.yaml'))

    # load all test plan yaml files
    for fname in sorted(flist):
        with open(os.path.join(testroot, fname), "rt", encoding='utf8') as file_handle:
            files[fname] = yaml.load(file_handle, Loader=yaml.CSafeLoader)

    if division + '/index.yaml' not in files:
        print('# Division has no index')
        return

    for group in files[division + '/index.yaml']:
        if group_filter is not None and group_filter != group['group']:
            continue
        print("---")
        group["kind"] = "group"
        print(yaml.dump(group, default_flow_style=False))
        for filename, content in files.items():
            if isinstance(content, dict) and content.get("group") == group["group"]:
                group_data = {"kind": "test", "filename": filename, "content": content}
                print("---")
                print(yaml.dump(group_data, default_flow_style=False))


if __name__ == "__main__":
    main()
