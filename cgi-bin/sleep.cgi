#!/usr/bin/python3

from urllib.parse import parse_qs
import os
import time

qs = parse_qs(os.getenv("QUERY_STRING", "t=10&ct=text/javascript"))

content_type = qs.get("ct", ["text/javascript"])[0]

if content_type == "text/javascript":
    prefix = "// "
else:
    prefix = ""

print("Content-Type: {}".format(content_type))
print("")

sleep_time = int(qs.get("t", [10])[0])

print("{}Sleeping for {} seconds".format(prefix, sleep_time))

time.sleep(sleep_time)

print("{}Done sleeping {} seconds".format(prefix,sleep_time))

