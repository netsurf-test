#!/usr/bin/python3

from urllib.parse import parse_qs
import cgi
import cgitb
import os
import sys
import io

cgitb.enable()

print("Content-Type: text/plain")
print("")

permitted_env = set([
    "DOCUMENT_ROOT",
    "QUERY_STRING",
    "GATEWAY_INTERFACE",
])

permitted_prefix = [
    "HTTP",
    "REQUEST_",
    "SERVER_",
    "REMOTE_",
    "SCRIPT_",
    "PATH_",
    "AUTH_",
    "CONTENT_",
]

for k in sorted(os.environ.keys()):
    if k in permitted_env or any((k.startswith(p) for p in permitted_prefix)):
        print("ENV:{}:{}".format(k, os.environ[k]))

if "QUERY_STRING" in os.environ:
    qs = parse_qs(os.getenv("QUERY_STRING"))
    for k, vs in qs.items():
        for i, v in enumerate(vs):
            if len(vs) > 1:
                idx = "[{}]".format(i)
            else:
                idx = ""
            print("QS:{}{}:{}".format(k, idx, v))

stdin_content = sys.stdin.buffer.read()

print("STDIN:START")
sys.stdout.flush()
sys.stdout.buffer.write(stdin_content)
print("STDIN:END")

valid_post_types = [
    "application/x-www-form-urlencoded",
    "multipart/form-data",
]

if os.environ.get("CONTENT_TYPE", "") in valid_post_types:
    stdin_io = io.BytesIO(stdin_content)
    fields = cgi.parse(stdin_io)
    for k in sorted(fields.keys()):
        print("POST:{}:{}".format(k, fields[k]))

print("END")


