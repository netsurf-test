#!/usr/bin/python3

import os
import sys
import io
import json
from html import escape as html_escape

print("Content-Type: text/html")
print("")

cookies = {}

if "HTTP_COOKIE" in os.environ:
   for cookie in map(str.strip, str.split(os.environ['HTTP_COOKIE'], ';')):
      (key, value) = cookie.split('=')
      cookies[key] = value

print("""
<html>
  <head><title>Cookie test</title></head>
  <body>
    <h1>Reflected cookies</h1>
    <ul>
""")

for k, v in cookies.items():
    print("      <li>{}: {}</li>".format(html_escape(k), html_escape(v)))
print("""
    </ul>
    <hr />
    <script>""")

print("    var uploaded_cookies = {};".format(json.dumps(cookies)))
print("""
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
      var counter = getCookie("counter");
      if (counter === "") { counter = "0"; }
      var uploaded_counter = uploaded_cookies["counter"] || "0";
      document.write("<b>Cookie:</b> " + counter + "<br/><b>JSON:</b> " + uploaded_counter + "<br />");
      console.error("Cookie: " + counter + " JSON: " + uploaded_counter);
      counter = parseInt(counter);
      counter = counter + 1;
      counter = counter.toString();
      document.cookie = "counter=" + counter;
    </script>
  </body>
</html>
""")


