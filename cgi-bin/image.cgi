#!/usr/bin/python3

from urllib.parse import parse_qs
import sys
import os
from io import BytesIO

auth = os.getenv("HTTP_AUTHORIZATION")
query = os.getenv("QUERY_STRING") or ""

query = parse_qs(query)

width = query.get("width", ["100"])[0]
height = query.get("height", ["100"])[0]
fmt = query.get("format", "png")[0]

try:
    width = int(width)
except:
    width = 100

try:
    height = int(height)
except:
    height = 100
    
width = width if width > 0 else 100
height = height if height > 0 else 100

width = width if width < 4000 else 4000
height = height if height < 4000 else 4000

FORMATS = {
    "png":  { "ctype": "image/png",  "ptype": "png" },
    "jpeg": { "ctype": "image/jpeg", "ptype": "jpeg" },
    "bmp":  { "ctype": "image/bmp",  "ptype": "bmp" },
    "ico":  { "ctype": "image/ico",  "ptype": "ico" },
    "gif":  { "ctype": "image/gif",  "ptype": "gif" },
    "webp": { "ctyle": "image/webp", "ptype": "webp" },
}

fmt = FORMATS.get(fmt)

if fmt is None:
    fmt = FORMATS["png"]

from PIL import Image

im = Image.new('RGB', (width, height))
im.putdata([(255,0,0)] * (width * height))

print("Content-Type: {}".format(fmt["ctype"]))
print("")
sys.stdout.flush()

imgbytes = BytesIO()
im.save(fp=imgbytes, format=fmt["ptype"])

sys.stdout.buffer.write(imgbytes.getvalue())
